package samples.calculator;

import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.AbstractButtonOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JTextFieldOperator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CalculatorPanelTest {

    // The buttons on the swing app
    private AbstractButtonOperator one, two, three, four, five, six, seven, eight, nine, zero,
            dot, clear, add, subtract, multiply, divide, equals;

    // The field on the swing app
    private JTextFieldOperator field;

    @BeforeClass(description = "Initialize our connection to the swing app")
    public void setUp() throws Exception {
        // Point Jemmy2 at the class that starts the swing app
        new ClassReference("samples.calculator.CalculatorPanel").startApplication();

        // Attach to the main frame
        JFrameOperator mainFrame = new JFrameOperator();

        // Initialize the buttons using the buttons text
        one = new AbstractButtonOperator(mainFrame, "1");
        two = new AbstractButtonOperator(mainFrame, "2");
        three = new AbstractButtonOperator(mainFrame, "3");
        four = new AbstractButtonOperator(mainFrame, "4");
        five = new AbstractButtonOperator(mainFrame, "5");
        six = new AbstractButtonOperator(mainFrame, "6");
        seven = new AbstractButtonOperator(mainFrame, "7");
        eight = new AbstractButtonOperator(mainFrame, "8");
        nine = new AbstractButtonOperator(mainFrame, "9");
        zero = new AbstractButtonOperator(mainFrame, "0");
        dot = new AbstractButtonOperator(mainFrame, ".");
        clear = new AbstractButtonOperator(mainFrame, "C");
        add = new AbstractButtonOperator(mainFrame, "+");
        subtract = new AbstractButtonOperator(mainFrame, "-");
        multiply = new AbstractButtonOperator(mainFrame, "*");
        divide = new AbstractButtonOperator(mainFrame, "/");
        equals = new AbstractButtonOperator(mainFrame, "=");

        // Initialize the only text field
        field = new JTextFieldOperator(mainFrame);
    }

    @BeforeTest(description = "Clear out the last results before running next test")
    public void clearField() throws Exception{
        try {
            clear.doClick();
        } catch (Exception e) {/* Ignore a NPE */}
    }

    @Test(description = "Assert 1 + 3 = 4")
    public void simpleAdditionTest() throws Exception {
        one.doClick();
        add.doClick();
        three.doClick();
        equals.doClick();
        Assert.assertEquals(field.getText(), "4");
    }

    @Test(description = "Assert 20 - 9 = 11")
    public void simpleSubtractionTest() throws Exception {
        two.doClick();
        zero.doClick();
        subtract.doClick();
        nine.doClick();
        equals.doClick();
        Assert.assertEquals(field.getText(), "11");
    }

    @Test(description = "Assert 7 * 8 = 56")
    public void simpleMultiplicationTest() throws Exception {
        seven.doClick();
        multiply.doClick();
        eight.doClick();
        equals.doClick();
        Assert.assertEquals(field.getText(), "56");
    }

    @Test(description = "Assert 6 / 4 = 1.5")
    public void simpleDivisionTest() throws Exception {
        six.doClick();
        divide.doClick();
        four.doClick();
        equals.doClick();
        Assert.assertEquals(field.getText(), "1.5");
    }

    @Test(description = "Assert clear button sets field to 0")
    public void clearFieldTest() throws Exception {
        five.doClick();
        dot.doClick();
        zero.doClick();
        Assert.assertEquals(field.getText(), "5.0");

        clear.doClick();
        Assert.assertEquals(field.getText(), "0");
    }
}
